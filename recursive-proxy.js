
class RecursiveProxy {
	constructor(target, configuration = {}) {
		this.path = [];
		this.curTarget
			= this._target
			= target;

		this.traps = [
			'apply',
			'construct',
			'defineProperty',
			'deleteProperty',
			'enumerate',
			'get',
			'getOwnPropertyDescriptor',
			'getPrototypeOf',
			'has',
			'isExtensible',
			'ownKeys',
			'preventExtensions',
			'set',
			'setPrototypeOf'
		];
		this.configure(configuration);

		this.p0 = new Proxy(this, this);
		this.proxy = new Proxy(this, this);

		return this.p0;
	}

	configure(configuration) {
		this.bindDefaults();
		var thisAction;

		Object.keys(configuration).forEach(function(action) {
			thisAction = this.traps.indexOf(action) !== -1
				? '_' + action
				: action;

			this[thisAction] = configuration[action];
		}, this);
	}

	bindDefaults() {
		this.traps
			.filter(action => action !== 'get')
			.forEach(this.bindAction, this);
	}

	bindAction(action) {
		this[action] = function(target, ...args) {
			return this['_' + action](this.curTarget, ...args);
		}
		this['_' + action] = function() {
			return Reflect[action](...arguments);
		}
	}

	get(target, prop, receiver) {
		var obj = receiver == this.p0
			? this.target
			: this.curTarget;

		return this.isObject(obj[prop])
			? this.subTarget(prop)
			: this._get(obj, prop);
	}
	_get(target, prop) {
		return Reflect.get(...arguments);
	}

	get target() {
		this.path = [];
		this.curTarget = this._target;

		return this._target;
	}

	subTarget(prop) {
		this.path.push(prop);
		this.curTarget = this.curTarget[prop];

		return this.getObject();
	}

	getObject() {
		return this.proxy;
	}

	isObject(val) {
		return val instanceof Object
			&& val.constructor.name === 'Object';
	}
}

if (typeof define === 'function' && define.amd) {
	define(function() {
		return window.RecursiveProxy = RecursiveProxy;
	});
} else if (typeof exports !== 'undefined') {
	module.exports = RecursiveProxy;
} else {
	window.RecursiveProxy = RecursiveProxy;
}
